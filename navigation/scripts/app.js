
// (function () {

   
//     // this function is called by Cordova when the application is loaded by the device
//     document.addEventListener('deviceready', function () {  
      
//       // hide the splash screen as soon as the app is ready. otherwise
//       // Cordova will wait 5 very long seconds to do it for you.
//       navigator.splashscreen.hide();

//       kendo.UserEvents.defaultThreshold(20);
// var app = new kendo.mobile.Application(document.body);
        
//     }, false);


// }());









(function (global) {
    var app = global.app = global.app || {};

    app.makeUrlAbsolute = function (url) {
            var anchorEl = document.createElement("a");
            anchorEl.href = url;
            return anchorEl.href;
        };

    document.addEventListener("deviceready", function () {
   //     cordova.plugins.Keyboard.disableScroll(true);
        navigator.splashscreen.hide();
        app.changeSkin = function (e) {
            var mobileSkin = "";

            if (e.sender.element.text() === "Flat") {
                e.sender.element.text("Native");
                mobileSkin = "flat";
            } else {
                e.sender.element.text("Flat");
                mobileSkin = "";
            }

            app.application.skin(mobileSkin);
        };     
     
        	kendo.UserEvents.defaultThreshold(20);
             app.application = new kendo.mobile.Application(document.body,{
             device: "iphone",       // Mobile device, can be "ipad", "iphone", "android", "fire", "blackberry", "wp"
              skin:"flat",    
            name: "ios",          // Mobile OS, can be "ios", "android", "blackberry", "wp"
            ios: true,            // Mobile OS name as a flag
            majorVersion: 8,      // Major OS version
            minorVersion: "0.0",  // Minor OS versions
            flatVersion: "500",   // Flat OS version for easier comparison
            appMode: true,       // Whether running in browser or in AppMode/Cordova/PhoneGap/Telerik AppBuilder.
            cordova: false,       // Whether running in Cordova/PhoneGap/Telerik AppBuilder.
            tablet: "ipad"        // If a tablet - tablet name or false for a phone.
     });
    }, false);
})(window);